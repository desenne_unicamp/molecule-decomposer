function getMolecule() {
  molecule = document.getElementById("molecule").value;
  formula = document.getElementById("molecule_formula");
  atom_list = document.getElementById("atoms_list")

  if (molecule.trim() == "") {
  	formula.style.display = "none";
  	atom_list.innerHTML = "";
  	atom_list.stype.display = "none";
  } else {
  	  formula.style.display = "block";
	  formula.innerHTML = "Atoms in "+molecule;

	  var xhttp = new XMLHttpRequest();
	  var path = "/api/?molecule="+molecule

	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
			ans = JSON.parse(xhttp.response);
			atom_list.innerHTML = "";
			atom_list.style.display = "block";

			for (const [key, value] of Object.entries(ans)) {
				console.log(`${key}: ${value}`);
				var li = document.createElement("li");
				li.innerHTML = `${key} -------> ${value}`;
				atom_list.appendChild(li);
			}
	    }
	  };
	  xhttp.open("GET", path, true);
	  xhttp.send();
  }
}