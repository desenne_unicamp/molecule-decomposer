# Carolina de Senne Garcia
# desennecarol@gmail.com

import collections
from http.server import HTTPServer,BaseHTTPRequestHandler
import json
from urllib.parse import urlparse

close_brackets = {']','}',')'}
open_brackets = {'[','{','(',}
brackets = {']','}',')','[','{','(',}


def parse(molecule):
	l = []
	lastDigit = False
	for c in molecule:
		if c.isalpha():
			if c.isupper():
				l.append(c)
			else:
				l[-1] = l[-1]+c
			lastDigit = False
		elif c in brackets:
			l.append(c)
			lastDigit = False
		else:
			if lastDigit:
				l[-1] = l[-1]+c
			else:
				l.append(c)
			lastDigit = True
	return l

def add_omitted_1s(molecule):
	n = len(molecule)
	aux = []
	for i in range(n):
		e = molecule[i]
		aux.append(e)
		if e[0].isalpha() or e[0] in close_brackets:
			if i+1==n or not molecule[i+1][0].isnumeric():
				aux.append('1')
	return aux

def atoms_in_molecule(molecule):
	better_molecule = add_omitted_1s(parse(molecule))
	better_molecule.reverse()

	indexes = []
	cumulative_index = 1
	atoms_count = collections.defaultdict(int)

	for e in better_molecule:
		if e[0].isnumeric():
			indexes.append(int(e))
			cumulative_index *= int(e)
			continue
		if e[0] in close_brackets:
			continue
		if e[0].isalpha():
			atoms_count[e] += cumulative_index
		# Here we have a opening bracket or an atomic element
		# Either way we need to pop an index from the stack
		i = indexes.pop()
		cumulative_index //= i

	return atoms_count

def test():
	atoms_in_molecule('K4[ON(SO3)2]2') # {'O': 14, 'S': 4, 'N': 2, 'K': 4}
	atoms_in_molecule('Mg(OH)2')	   # {'H': 2, 'O': 2, 'Mg': 1}
	atoms_in_molecule('H2O')		   # {'O': 1, 'H': 2}

class CarolsHandler(BaseHTTPRequestHandler):

	def do_GET(self):
		if self.path.startswith('/api'):
			self.do_get_molecule()
		else:
			self.do_serve_file()

	def do_get_molecule(self):
		self.send_response(200)
		self.send_header('Content-type', 'application/json')
		self.end_headers()

		query = urlparse(self.path).query
		query_components = dict(qc.split("=") for qc in query.split("&"))
		molecule = query_components["molecule"]
		atoms = atoms_in_molecule(molecule)
		ans = json.dumps(atoms)
		print(ans)
		self.wfile.write(ans.encode())

	def do_serve_file(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()
		path = self.path
		if path == '/':
			path = '/index.html'
		self.wfile.write(open(path[1:], 'rb').read())


if __name__ == '__main__':
	httpd = HTTPServer(('0.0.0.0', 8080), CarolsHandler)
	httpd.serve_forever()




