# Molecule Decomposer

Project proposed by @ahollocou (find the description [here](https://gist.github.com/ahollocou/f454f41718ef6c179617beb4c576ea4c)).
You can access the webpage with the API on http://34.73.237.34:8080/ . Obs: the IP address might change, so please contact me in case you have problems accessing the page.

### API

The aim of this project was to create and deploy a simple API to decompose a molecule formula into it's elements and the number of atoms for each element.

For example:

The input `'H2O'` must return `{'H': 2, 'O': 1}`

The input `'Mg(OH)2'` must return `{'Mg': 1, 'O': 2, 'H': 2}`

The input `'K4[ON(SO3)2]2'` must return `{'K': 4, 'O': 14, 'N': 2, 'S': 4}`

As you can see, some formulas have brackets in them. The index outside the brackets tells you that you have to multiply count of each atom inside the bracket on this index.

For example, in `Fe(NO3)2` you have one iron atom, two nitrogen atoms and six oxygen atoms.

Note that brackets may be round, square or curly and can also be nested. Index after the braces is optional.

### Frontend

It consists of a web page with a search bar where users can enter a molecule formula.

When users validate their input, the app calls the API deployed above and displays the number of atoms of each element contained in the molecule.
